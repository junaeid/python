"""
 Write a Python program to accept a filename from the user and print the extension of that.
Sample filename : abc.java
Output : java
"""

x = input("Enter filename: ")
first, last = x.split(".")
print(last)
