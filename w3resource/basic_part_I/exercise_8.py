"""
Write a Python program to display the first and last colors from the following list.
color_list = ["Red","Green","White" ,"Black"]
"""

color_list = ["Red", "Green", "White", "Black"]
first, *others, last = color_list

print(f"first color {first} last color {last}")